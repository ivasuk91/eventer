class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :edit]

  def index
    @events = Event.all.order("created_at DESC")
  end

  def show
  end

  def new
    @event = Event.new
  end

  def edit
  end

  def create
    @event = Event.new(event_params)
    if @event.save
      flash[:notice] = "Event has been created successfully!"
      redirect_to root_path
    else
      render 'new'
    end
  end

  def update
    if @event.update(event_params)
      flash[:notice] = "Event has been updated successfully!"
      redirect_to event_path(@event)
    else
      flash[:error] = 'Event has not been updated !'
      render 'edit'
    end
  end

  def destroy
    if @event.destroy
      flash[:notice] = "Event has been deleted successfully!"
      redirect_to root_path
    else
      flash[:error] = "Event has not been deleted"
      redirect_to root_path
    end
  end

  private

  def set_event
    @event = Event.find(params[:id])
  end

  def event_params
    params.require(:event).permit(:name, :place, :description, :date)
  end
end
