class Event < ApplicationRecord
  has_many :registrations
  has_many :attendees, through: :registrations
  has_many :comments
end

